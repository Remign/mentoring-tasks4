﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Task2
{
	public interface ICache<T>
	{
		IEnumerable<T> Get(string forUser);
    void Set(string forUser, IEnumerable<T> categories, CacheItemPolicy policy = null, DateTimeOffset? absoluteExpiration = null);
	}
}
