﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NorthwindLibrary;

namespace Task2
{
  class Program
  {
    static void Main(string[] args)
    {
      MemoryCache();
    }

    
    public static void MemoryCache()
    {
      //RUN THIS SCRIPT FIRST:
      //ALTER DATABASE database_name SET TRUSTWORTHY ON WITH ROLLBACK IMMEDIATE
      //ALTER DATABASE database_name SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE
      //ALTER AUTHORIZATION ON DATABASE::database_name TO sa

      var categoryManager = new CategoriesManager(new MemoryCache<Category>());

      for (var i = 0; i < 10; i++)
      {
        if (i == 4 || i == 8)
        {
          Console.WriteLine(categoryManager.GetCategories(go : true).Count());
        }
        else
        {
          Console.WriteLine(categoryManager.GetCategories().Count());
        }
        
        Thread.Sleep(100);
      }
    }

    
    //public void RedisCache()
    //{
    //  var categoryManager = new CategoriesManager(new RedisCache("localhost"));

    //  for (var i = 0; i < 10; i++)
    //  {
    //    Console.WriteLine(categoryManager.GetCategories().Count());
    //    Thread.Sleep(100);
    //  }
    //}
  }
}
