﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.Caching;
using NorthwindLibrary;

namespace Task2
{
  internal class MemoryCache<T> : ICache<T>
	{
	  readonly ObjectCache _cache = MemoryCache.Default;
	  readonly string _prefix  = "Cache_" + typeof(T);

		public IEnumerable<T> Get(string forUser)
		{
			return (IEnumerable<T>) _cache.Get(_prefix + forUser);
		}

		public void Set(string forUser, IEnumerable<T> categories, CacheItemPolicy policy = null, DateTimeOffset? absoluteExpiration = null)
		{
		  if (absoluteExpiration == null && policy == null)
		  {
        _cache.Set(_prefix + forUser, categories, ObjectCache.InfiniteAbsoluteExpiration);
		  }
      else if (policy != null)
      {
        _cache.Set(_prefix + forUser, categories, policy);
      }
      else
      {
        _cache.Set(_prefix + forUser, categories, absoluteExpiration.Value);
      }
		}
	}
}
