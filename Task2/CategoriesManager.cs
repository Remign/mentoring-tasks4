﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Threading;
using NorthwindLibrary;

namespace Task2
{
  public class CategoriesManager
  {
    private readonly ICache<Category> _cache;
    private readonly CacheItemPolicy _policy;
    private readonly string _name = "Cache_Category";

    private SqlCommand _goCommand;

    public CategoriesManager(ICache<Category> cache)
    {
      _cache = cache;
    }

    public IEnumerable<Category> GetCategories(bool changePolicy = true, bool go = false)
    {
      if (go)
      {
        _goCommand.ExecuteNonQuery();
        _goCommand.Connection.Dispose();
        _goCommand.Dispose();
      }

      Console.WriteLine("Get Categories");

      var user = _name;
      var categories = _cache.Get(user);

      if (categories == null)
      {
        Console.WriteLine("From DB");

        using (var dbContext = new Northwind())
        {
          dbContext.Configuration.LazyLoadingEnabled = false;
          dbContext.Configuration.ProxyCreationEnabled = false;
          categories = dbContext.Categories.ToList();

          if (changePolicy)
          {
            var connectionString = dbContext.Database.Connection.ConnectionString;

            SqlDependency.Start(connectionString);
            var policy = new CacheItemPolicy();

            var conn = new SqlConnection(connectionString);

            var categoriesQueryToDropCache = dbContext.Categories.Take(5);
            var cmd = new SqlCommand(categoriesQueryToDropCache.ToString(), conn);

            cmd.Notification = null;

            var dep = new SqlDependency();
            dep.AddCommandDependency(cmd);

            conn.Open();

            policy.ChangeMonitors.Add(new SqlChangeMonitor(dep));

            _cache.Set(user, categories, policy);

            _goCommand = cmd;

          }
          else
          {
            _cache.Set(user, categories, absoluteExpiration: DateTime.UtcNow.AddSeconds(10));
          }
        }
      }

      return categories;
    }

  }
}
