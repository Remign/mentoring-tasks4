﻿using System.IO;
using System.Runtime.Serialization;
using StackExchange.Redis;

namespace Task1
{
  class FibonacciRedisCache : IFibonacciCache
  {
    private readonly ConnectionMultiplexer _redisConnection;
    private const string Prefix = "Cache_Fibonacci";

    readonly DataContractSerializer _serializer = new DataContractSerializer(
      typeof(int));

    public FibonacciRedisCache(string hostName)
    {
      _redisConnection = ConnectionMultiplexer.Connect(hostName);
    }

    public int? Get(int target)
    {
      var db = _redisConnection.GetDatabase();
      byte[] s = db.StringGet(Prefix + target);

      if (s == null) return null;

      return (int?)_serializer
        .ReadObject(new MemoryStream(s));
    }

    public void Set(int target, int value)
    {
      var db = _redisConnection.GetDatabase();
      var key = Prefix + target;

      var stream = new MemoryStream();
      _serializer.WriteObject(stream, value);
      db.StringSet(key, stream.ToArray());
    }
  }
}