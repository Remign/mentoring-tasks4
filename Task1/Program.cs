﻿using System;

namespace Task1
{
  class Program
  {
    private static readonly int[] TestValues = {22, 23, 15};

    static void Main(string[] args)
    {
      TestFibonacciManager(new FibonacciManager(new FibonacciMemoryCache()));

      //TUTORIAL HOW TO SET UP REDIS ENVINRONMENT:
      //http://www.codeproject.com/Articles/636730/Distributed-Caching-using-Redis

      TestFibonacciManager(new FibonacciManager(new FibonacciRedisCache("localhost")));
    }

    static void TestFibonacciManager(FibonacciManager manager)
    {
      Console.WriteLine("Type : {0}", manager.GetManagerType());
      foreach (var testValue in TestValues)
      {
        Console.WriteLine("Result = {0}", manager.GetFibonacci(testValue));
      }
    }
  }
}
