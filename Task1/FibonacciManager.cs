﻿using System;

namespace Task1
{
  public class FibonacciManager
  {
    private readonly IFibonacciCache _cache;

    public FibonacciManager(IFibonacciCache cache)
    {
      _cache = cache;
    }

    public int GetFibonacci(int target)
    {
      Console.WriteLine("Processing Fibonacci of {0} index", target);

      var currentVal = _cache.Get(target);

      if (currentVal == null)
      {
        var newValue = target > 1 ? GetFibonacci(target - 1) + GetFibonacci(target - 2) : target;

        _cache.Set(target, newValue);

        return newValue;
      }

      Console.WriteLine("Get Fibonacci value of {0} index from cache", target);
      return currentVal.Value;
    }

    public string GetManagerType()
    {
      return _cache.GetType().ToString();
    }
  }
}