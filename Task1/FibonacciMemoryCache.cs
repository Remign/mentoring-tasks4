﻿using System.Runtime.Caching;

namespace Task1
{
  internal class FibonacciMemoryCache : IFibonacciCache
  {
    private readonly ObjectCache _cache = MemoryCache.Default;
    private const string Prefix = "Cache_Fibonacci";

    public void Set(int target, int value)
    {
      _cache.Set(Prefix + target, value, ObjectCache.InfiniteAbsoluteExpiration);
    }

    public int? Get(int target)
    {
      return (int?)_cache.Get(Prefix + target);
    }
  }
}