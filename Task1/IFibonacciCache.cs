﻿namespace Task1
{
  public interface IFibonacciCache
  {
    int? Get(int target);
    void Set(int target, int value); 
  }
}